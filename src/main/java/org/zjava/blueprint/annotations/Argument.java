package org.zjava.blueprint.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 *
 * @author Michał Blinkiewicz
 */
@Documented
@Target(ElementType.ANNOTATION_TYPE)
@Retention(RetentionPolicy.SOURCE)
public @interface Argument {
    String index() default "";
    String type() default "";
    String ref() default "";
    String value() default "";

//    - Either all arguments have a specified index or none have a specified index.
//    - If indexes are specified, they must be unique and run from 0..(n-1), where n is the number of arguments.
//    - The following attributes and elements are mutually exclusive:
//        - ref
//        - value
//        - An inlined object value
}
