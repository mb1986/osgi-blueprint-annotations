package org.zjava.blueprint.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 *
 * @author Michał Blinkiewicz
 */
@Documented
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.SOURCE)
public @interface Bean {
    String id() default "";
    String activation() default ""; // = eager only if scope == singleton

    String scope() default "";
    String[] dependsOn() default {};
}
