package org.zjava.blueprint.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 *
 * @author Michał Blinkiewicz
 */
@Documented
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.SOURCE)
public @interface Blueprint {
    String resourceName() default "blueprint.xml";

    String defaultActivation() default ""; // Activation.EAGER
    String defaultAvailability() default ""; // Availability.MANDATORY
    String defaultTimeout() default ""; // "30000"
}
