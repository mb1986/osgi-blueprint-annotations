package org.zjava.blueprint.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 *
 * @author Michał Blinkiewicz
 */
@Documented
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.SOURCE)
public @interface Property {
    String name();
    String ref() default "";
    String value() default "";

    //<property name="foo.bar.baz" value="42"/>

//    - The following attributes/elements are mutually exclusive
//        - ref
//        - value
//        - An inlined object value
}
