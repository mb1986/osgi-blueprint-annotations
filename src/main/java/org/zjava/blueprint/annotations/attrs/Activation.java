package org.zjava.blueprint.annotations.attrs;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author Michał Blinkiewicz
 */
public final class Activation {
    
    public static final String LAZY = "lazy";
    public static final String EAGER = "eager";

    @SuppressWarnings("InitializerMayBeStatic")
    public static final Set<String> PERMITTED = Collections.unmodifiableSet(new HashSet<String>(){{
        add(LAZY);
        add(EAGER);
    }});

}
