package org.zjava.blueprint.annotations.attrs;

/**
 *
 * @author Michał Blinkiewicz
 */
public final class Availability {
    
    public static final String MANDATORY = "eager";
    public static final String OPTIONAL = "lazy";

}
