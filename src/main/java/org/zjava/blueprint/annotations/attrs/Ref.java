package org.zjava.blueprint.annotations.attrs;

/**
 *
 * @author Michał Blinkiewicz
 */
public final class Ref { // FIXME --- should be reviewed
    
    public static final String BLUEPRINT_CONTAINER = "blueprintContainer";
    public static final String BLUEPRINT_BUNDLE = "blueprintBundle";
    public static final String BLUEPRINT_BUNDLE_CONTEXT = "blueprintBundleContext";
    public static final String BLUEPRINT_CONVERTER = "blueprintConverter";

}
