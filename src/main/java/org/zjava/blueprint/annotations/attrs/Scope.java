package org.zjava.blueprint.annotations.attrs;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author Michał Blinkiewicz
 */
public final class Scope {
    
    public static final String SINGLETON = "singleton";
    public static final String PROTOTYPE = "prototype";

    @SuppressWarnings("InitializerMayBeStatic")
    public static final Set<String> PERMITTED = Collections.unmodifiableSet(new HashSet<String>(){{
        add(SINGLETON);
        add(PROTOTYPE);
    }});

}
