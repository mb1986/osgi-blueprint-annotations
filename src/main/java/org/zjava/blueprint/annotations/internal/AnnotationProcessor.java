package org.zjava.blueprint.annotations.internal;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import javax.lang.model.element.AnnotationMirror;
import javax.lang.model.element.AnnotationValue;
import javax.lang.model.element.Element;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.TypeElement;

/**
 *
 * @author Michał Blinkiewicz
 */
public abstract class AnnotationProcessor {

    private final List<ProcessingError> errors = new ArrayList<>();

    public abstract void process(TypeElement type, Element annotated, AnnotationMirror annotation,
            Map<? extends ExecutableElement, ? extends AnnotationValue> values);

    protected final void error(ProcessingError error) {
        errors.add(error);
    }

    public final List<ProcessingError> getErrors() {
        return Collections.unmodifiableList(errors);
    }

    public final void clearErros() {
        errors.clear();
    }

}
