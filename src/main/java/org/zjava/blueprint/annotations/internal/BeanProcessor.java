package org.zjava.blueprint.annotations.internal;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.AnnotationMirror;
import javax.lang.model.element.AnnotationValue;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.TypeElement;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.zjava.blueprint.annotations.Bean;
import org.zjava.blueprint.annotations.DestroyMethod;
import org.zjava.blueprint.annotations.Factory;
import org.zjava.blueprint.annotations.FactoryMethod;
import org.zjava.blueprint.annotations.InitMethod;
import org.zjava.blueprint.annotations.Inject;
import org.zjava.blueprint.annotations.Properties;
import org.zjava.blueprint.annotations.Property;
import org.zjava.blueprint.annotations.attrs.Id;
import org.zjava.blueprint.annotations.internal.utils.AnnotatedElement;
import org.zjava.blueprint.annotations.internal.utils.ModelUtils;
import org.zjava.blueprint.annotations.internal.utils.NameUtils;
import org.zjava.blueprint.annotations.internal.xml.BeanXml;
import org.zjava.blueprint.annotations.internal.xml.PropertyXml;
import org.zjava.blueprint.annotations.internal.xml.ValidationError;
import org.zjava.blueprint.annotations.internal.xml.ValidatorException;

/**
 *
 * @author Michał Blinkiewicz
 */
@RequiredArgsConstructor
public class BeanProcessor extends AnnotationProcessor {

    private final ProcessingEnvironment env;
    private final BlueprintContext context;

    @Override
    public void process(TypeElement type, Element annotated, AnnotationMirror annotation,
            Map<? extends ExecutableElement, ? extends AnnotationValue> values) {

        final TypeElement beanClass;
        if (annotated.getKind().isClass()) {
            beanClass = (TypeElement) annotated;
        } else {
            error(new ProcessingError("'Bean' annotated element is not a class!", annotated, annotation));
            return;
        }

        final Bean bean = beanClass.getAnnotation(Bean.class);
        final Factory factory = beanClass.getAnnotation(Factory.class);

        final BeanXml.Validator beanXml = new BeanXml.Validator();

        // id
        if (StringUtils.isBlank(bean.id())) {
            beanXml.setId(NameUtils.classNameToBeanId(beanClass.getSimpleName()));
        } else if (!bean.id().equals(Id.NONE)) {
            beanXml.setId(bean.id());
        }

        // depends-on
        beanXml.setDependsOn(StringUtils.join(bean.dependsOn(), ' '));

        // class / factory-ref, factory-method
        if (factory == null) {
            beanXml.setClazz(beanClass.getQualifiedName().toString());
        } else {
            beanXml.setFactoryComponent(factory.ref());
            beanXml.setFactoryMethod(factory.method());
        }

        // activation
        beanXml.setActivation(bean.activation());

        // scope
        beanXml.setScope(bean.scope());

        // factory-method
        processFactoryMethod(beanClass, factory, beanXml);

        // init-method, destroy-method
        processInitDestroyMethods(beanClass, beanXml);

        // injects
        processProperties(beanClass, beanXml);

        // arguments
        // TODO --- implement
            
        // validate and populate context
        try {
            context.addBean(beanXml.validate(), beanClass);
        } catch (ValidatorException ex) {
            for (ValidationError e : ex.getErrors()) {
                error(new ProcessingError(e.getMessage(), annotated, annotation));
            }
        } catch (ProcessorException ex) {
            for (ProcessingError e : ex.getErrors()) {
                error(e);
            }
        }
    }

    private void processFactoryMethod(TypeElement beanClass, Factory factory, BeanXml.Validator beanXml) {
        final AnnotatedElement factoryMethod = getOneMethod(beanClass, FactoryMethod.class.getName());
        if (factoryMethod != null) {
            if (factory == null) {
                beanXml.setFactoryMethod(factoryMethod.getElement().getSimpleName().toString());
            } else {
                error(new ProcessingError("Factory annotation is specified!",
                        factoryMethod.getElement(), factoryMethod.getAnnotation()));
            }
        }
    }

    private void processInitDestroyMethods(TypeElement beanClass, BeanXml.Validator beanXml) {
        final AnnotatedElement initMethod = getOneMethod(beanClass, InitMethod.class.getName());
        final AnnotatedElement destroyMethod = getOneMethod(beanClass, DestroyMethod.class.getName());
        if (initMethod != null) {
            beanXml.setInitMethod(initMethod.getElement().getSimpleName().toString());
        }
        if (destroyMethod != null) {
            beanXml.setDestroyMethod(destroyMethod.getElement().getSimpleName().toString());
        }
    }

    private void processProperties(TypeElement beanClass, BeanXml.Validator beanXml) {
        // injects
        final TypeElement injectType = env.getElementUtils().getTypeElement(Inject.class.getName());

        final List<AnnotatedElement> fieldInjects =
                ModelUtils.getAnnotatedEnclosedElements(beanClass, ElementKind.FIELD, injectType);

        final List<AnnotatedElement> setterInjects =
                ModelUtils.getAnnotatedEnclosedElements(beanClass, ElementKind.METHOD, injectType);

        final List<PropertyXml> propertyList = new ArrayList<>();

        // field
        for (final AnnotatedElement fieldInject : fieldInjects) {
            final Inject inject = fieldInject.getElement().getAnnotation(Inject.class);
            final PropertyXml.Validator propertyValidator = new PropertyXml.Validator();
            propertyValidator.setName(fieldInject.getElement().getSimpleName().toString());
            propertyValidator.setRef(inject.ref());
            propertyValidator.setValue(inject.value());
            try {
                propertyList.add(propertyValidator.validate());
            } catch (ValidatorException ex) {
                for (ValidationError ve : ex.getErrors()) {
                    error(new ProcessingError(ve.getMessage(),
                            fieldInject.getElement(), fieldInject.getAnnotation()));
                }
            }
        }
        // field
        for (final AnnotatedElement setterInject : setterInjects) {
            final String methodName = setterInject.getElement().getSimpleName().toString();
            final String name;
            if (methodName.length() > 3) {
                name = methodName.substring(3).substring(0, 1).toLowerCase() + methodName.substring(4);
            } else {
                name = "";
            }
            final Inject inject = setterInject.getElement().getAnnotation(Inject.class);
            final PropertyXml.Validator propertyValidator = new PropertyXml.Validator();
            propertyValidator.setName(name);
            propertyValidator.setRef(inject.ref());
            propertyValidator.setValue(inject.value());
            try {
                propertyList.add(propertyValidator.validate());
            } catch (ValidatorException ex) {
                for (ValidationError ve : ex.getErrors()) {
                    error(new ProcessingError(ve.getMessage(),
                            setterInject.getElement(), setterInject.getAnnotation()));
                }
            }
        }
        // property, properties
        final Properties properties = beanClass.getAnnotation(Properties.class);
        final Property property = beanClass.getAnnotation(Property.class);
        if (properties != null && property != null) {
            error(new ProcessingError("Conflicting 'Property' annotation!", beanClass,
                    ModelUtils.getAnnotationMirror(beanClass, Properties.class.getName())));
            error(new ProcessingError("Conflicting 'Properties' annotation!", beanClass,
                    ModelUtils.getAnnotationMirror(beanClass, Property.class.getName())));
        } else {
            if (properties != null) { // properties
                for (final Property prop : properties.value()) {
                    final PropertyXml.Validator propertyValidator = new PropertyXml.Validator();
                    propertyValidator.setName(prop.name());
                    propertyValidator.setRef(prop.ref());
                    propertyValidator.setValue(prop.value());
                    try {
                        propertyList.add(propertyValidator.validate());
                    } catch (ValidatorException ex) {
                        for (ValidationError ve : ex.getErrors()) {
                            error(new ProcessingError(ve.getMessage(), beanClass,
                                    ModelUtils.getAnnotationMirror(beanClass, Properties.class.getName())));
                        }
                    }
                }
            } else if (property != null) { // property
                final PropertyXml.Validator propertyValidator = new PropertyXml.Validator();
                propertyValidator.setName(property.name());
                propertyValidator.setRef(property.ref());
                propertyValidator.setValue(property.value());
                try {
                    propertyList.add(propertyValidator.validate());
                } catch (ValidatorException ex) {
                    for (ValidationError ve : ex.getErrors()) {
                        error(new ProcessingError(ve.getMessage(), beanClass,
                                ModelUtils.getAnnotationMirror(beanClass, Property.class.getName())));
                    }
                }
            }
        }

        if (!propertyList.isEmpty()) {
            beanXml.setProperties(propertyList);
        }
    }

    private AnnotatedElement getOneMethod(TypeElement enclosing, String annotation) {
        final List<AnnotatedElement> methods =
                ModelUtils.getAnnotatedEnclosedElements(enclosing, ElementKind.METHOD,
                        env.getElementUtils().getTypeElement(annotation));
        if (methods.size() == 1) {
            return methods.iterator().next();
        } else {
            for (AnnotatedElement ae : methods) {
                error(new ProcessingError("Multiple '"
                                + ae.getAnnotation().getAnnotationType().asElement().getSimpleName()
                                + "' methods are not supported!",
                        ae.getElement(), ae.getAnnotation()));
            }
            return null;
        }
    }

}
