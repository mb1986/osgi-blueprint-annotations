package org.zjava.blueprint.annotations.internal;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javax.lang.model.element.Element;
import org.zjava.blueprint.annotations.internal.xml.BeanXml;
import org.zjava.blueprint.annotations.internal.xml.BlueprintXml;

/**
 *
 * @author Michał Blinkiewicz
 */
public class BlueprintContext {

    private final Map<String, BeanXml> beans = new HashMap<>();

    public void addBean(BeanXml block, Element e) throws ProcessorException {
        if (beans.containsKey(block.getId())) { // FIXME --- is id blank?
            BeanXml other = beans.get(block.getId());
            throw new ProcessorException(
                    new ProcessingError("There is another bean with the same id '" + block.getId() + "'!", e),
                    new ProcessingError("There is another bean with the same id '" + other.getId() + "'!", e));
        } else {
            beans.put(block.getId(), block);
        }
    }

    public BlueprintXml getBlueprintXml() {
        return new BlueprintXml(new ArrayList<>(beans.values()));
    }

}
