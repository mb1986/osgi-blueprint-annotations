package org.zjava.blueprint.annotations.internal;

import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.net.URL;
import java.util.Date;
import javax.tools.FileObject;
import javax.tools.StandardLocation;
import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import org.xml.sax.SAXException;
import org.zjava.blueprint.annotations.Argument;
import org.zjava.blueprint.annotations.Arguments;
import org.zjava.blueprint.annotations.Bean;
import org.zjava.blueprint.annotations.Blueprint;
import org.zjava.blueprint.annotations.DestroyMethod;
import org.zjava.blueprint.annotations.Environment;
import org.zjava.blueprint.annotations.Factory;
import org.zjava.blueprint.annotations.FactoryMethod;
import org.zjava.blueprint.annotations.InitMethod;
import org.zjava.blueprint.annotations.Inject;
import org.zjava.blueprint.annotations.Properties;
import org.zjava.blueprint.annotations.Property;
import org.zjava.blueprint.annotations.Reference;
import org.zjava.blueprint.annotations.ReferenceList;
import org.zjava.blueprint.annotations.Service;
import org.zjava.blueprint.annotations.internal.prompters.GlobalPrompter;
import org.zjava.blueprint.annotations.internal.prompters.Prompters;
import org.zjava.blueprint.annotations.internal.validators.BeanExistenceValidator;
import org.zjava.blueprint.annotations.internal.validators.InjectValidator;
import org.zjava.blueprint.annotations.internal.validators.PropertiesValidator;
import org.zjava.blueprint.annotations.internal.validators.PropertyValidator;
import org.zjava.blueprint.annotations.internal.xml.BlueprintXml;

/**
 *
 * @author Michał Blinkiewicz
 */
public class BlueprintProcessor extends ProcessorEngine {

    private final java.util.Properties projectProperties = new java.util.Properties();
    private final BlueprintContext context = new BlueprintContext();

    @Override
    public void init() {
        try {
            final FileObject projectPropertiesFile =
                    processingEnv.getFiler().getResource(StandardLocation.CLASS_PATH, "", "project.properties");
            try (final Reader reader = projectPropertiesFile.openReader(false)) {
                projectProperties.load(reader);
            }
        } catch (IOException ignore) {
        }
    }

    @Override
    public void configure(Processors processors, Prompters prompters) {
        processors.add(Bean.class.getName(), new BeanProcessor(processingEnv, context));
        processors.add(Inject.class.getName(), new InjectValidator());
        processors.add(Property.class.getName(), new PropertyValidator());
        processors.add(Properties.class.getName(), new PropertiesValidator());

        final BeanExistenceValidator beanExistence = new BeanExistenceValidator();
        processors.add(Factory.class.getName(), beanExistence);
        processors.add(FactoryMethod.class.getName(), beanExistence);
        processors.add(InitMethod.class.getName(), beanExistence);
        processors.add(DestroyMethod.class.getName(), beanExistence);
        processors.add(Inject.class.getName(), beanExistence);
        processors.add(Property.class.getName(), beanExistence);

        final NotSupportedYetProcessor notSupported = new NotSupportedYetProcessor();
        processors.add(Arguments.class.getName(), notSupported);
        processors.add(Argument.class.getName(), notSupported);
        processors.add(Blueprint.class.getName(), notSupported);
        processors.add(Environment.class.getName(), notSupported);
        processors.add(Reference.class.getName(), notSupported);
        processors.add(ReferenceList.class.getName(), notSupported);
        processors.add(Service.class.getName(), notSupported);

        prompters.setGlobalPrompter(new GlobalPrompter(processingEnv));
    }

    @Override
    public void after(boolean errors) {
        if (!errors) {
            try {
                final FileObject file =
                        processingEnv.getFiler().createResource(StandardLocation.CLASS_OUTPUT, "", "OSGI-INF/blueprint/generated.xml");
                try (final Writer writer = file.openWriter()) {
                    SchemaFactory sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
                    Schema schema = sf.newSchema(new URL(BlueprintXml.SCHEMA_LOCATION));
                    
                    JAXBContext jc = JAXBContext.newInstance(BlueprintXml.class);
                    Marshaller m = jc.createMarshaller();
                    m.setSchema(schema);
                    m.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
                    m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
                    m.setProperty(Marshaller.JAXB_SCHEMA_LOCATION, BlueprintXml.NAMESPACE_SCHEMA_LOCATION);
                    m.setProperty("com.sun.xml.internal.bind.xmlHeaders",
                            "<!-- OSGi Blueprint Annotations ("
                                    + projectProperties.getProperty("project.version", "Not known version!")
                                    + ") / Generated on " + new Date().toString() + " -->\n");
                    
                    m.marshal(context.getBlueprintXml(), writer);
                }
            } catch (JAXBException | SAXException | IOException ex) { // FIXME
                throw new IllegalStateException(ex);
            }
        } else {
            error("There are processing errors - could not generate blueprint XML document!");
        }
    }
    
}