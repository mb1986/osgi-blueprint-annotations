package org.zjava.blueprint.annotations.internal;

import java.util.Map;
import javax.lang.model.element.AnnotationMirror;
import javax.lang.model.element.AnnotationValue;
import javax.lang.model.element.Element;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.TypeElement;

/**
 *
 * @author mb
 */
public class NotSupportedYetProcessor extends AnnotationProcessor {

    @Override
    public void process(TypeElement type, Element annotated, AnnotationMirror annotation,
            Map<? extends ExecutableElement, ? extends AnnotationValue> values) {
        error(new ProcessingError("This annotation is not supported yet!", annotated, annotation));
    }
    
}
