package org.zjava.blueprint.annotations.internal;

import javax.lang.model.element.AnnotationMirror;
import javax.lang.model.element.AnnotationValue;
import javax.lang.model.element.Element;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

/**
 *
 * @author Michał Blinkiewicz
 */
@RequiredArgsConstructor
public class ProcessingError {

    @Getter @NonNull private final String message;
    @Getter private final Element element;
    @Getter private final AnnotationMirror annotationMirror;
    @Getter private final AnnotationValue annotationValue;

    public ProcessingError(String message) {
        this(message, null, null, null);
    }
    
    public ProcessingError(String message, Element element) {
        this(message, element, null, null);
    }
    
    public ProcessingError(String message, Element element, AnnotationMirror annotationMirror) {
        this(message, element, annotationMirror, null);
    }
    
}
