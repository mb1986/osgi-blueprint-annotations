package org.zjava.blueprint.annotations.internal;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.Completion;
import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.RoundEnvironment;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.AnnotationMirror;
import javax.lang.model.element.Element;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.TypeElement;
import javax.tools.Diagnostic;
import lombok.NonNull;
import org.zjava.blueprint.annotations.internal.prompters.Prompter;
import org.zjava.blueprint.annotations.internal.prompters.Prompters;
import org.zjava.blueprint.annotations.internal.utils.ModelUtils;

/**
 *
 * @author Michał Blinkiewicz
 */
public abstract class ProcessorEngine extends AbstractProcessor {

    private final Processors processors = new Processors();
    private final Prompters prompters = new Prompters();
    private boolean errors = false;

    @Override
    public synchronized void init(ProcessingEnvironment processingEnv) {
        super.init(processingEnv);
        init();
        configure(processors, prompters);
    }

    public abstract void init();
    public abstract void configure(Processors processors, Prompters prompters);

    public abstract void after(boolean errors);

    @Override
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
        if (!roundEnv.processingOver()) {
            for (TypeElement type : annotations) {
                for (Element annotated : roundEnv.getElementsAnnotatedWith(type)) {
                    for (AnnotationMirror mirror : annotated.getAnnotationMirrors()) {
                        if (mirror.getAnnotationType().asElement().equals(type)) {
                            final List<AnnotationProcessor> processorsList =
                                    processors.lookup(type.getQualifiedName().toString());
                            if (processorsList != null && !processorsList.isEmpty()) {
                                for (final AnnotationProcessor processor : processorsList) {
                                    processor.process(type, annotated, mirror,
                                            processingEnv.getElementUtils().getElementValuesWithDefaults(mirror));
                                    if (!processor.getErrors().isEmpty()) {
                                        errors = true;
                                        error(processor.getErrors());
                                        processor.clearErros();
                                    }
                                }
                            } else {
                                processingEnv.getMessager().printMessage(Diagnostic.Kind.ERROR,
                                        "Can not find any processor for this annotation type!", annotated, mirror);
                            }
                        }
                    }
                }
            }
        } else {
            after(errors);
        }
        return true;
    }

    @Override
    public Set<String> getSupportedAnnotationTypes() {
        return processors.getAnnotationTypes();
    }

    protected void error(@NonNull String msg) {
        processingEnv.getMessager().printMessage(Diagnostic.Kind.ERROR, msg);
    }

    protected void error(@NonNull ProcessingError error) {
        if (error.getElement() != null) {
            if (error.getAnnotationMirror() != null) {
                if (error.getAnnotationValue() != null) {
                    processingEnv.getMessager().printMessage(Diagnostic.Kind.ERROR,
                            error.getMessage(),
                            error.getElement(),
                            error.getAnnotationMirror(),
                            error.getAnnotationValue());
                    return;
                } else {
                    processingEnv.getMessager().printMessage(Diagnostic.Kind.ERROR,
                            error.getMessage(),
                            error.getElement(),
                            error.getAnnotationMirror());
                    return;
                }
            } else {
                processingEnv.getMessager().printMessage(Diagnostic.Kind.ERROR,
                        error.getMessage(),
                        error.getElement());
                return;
            }
        }
        processingEnv.getMessager().printMessage(Diagnostic.Kind.ERROR, error.getMessage());
    }

    protected void error(@NonNull List<ProcessingError> errors) {
        for (final ProcessingError e: errors) {
            error(e);
        }
    }

    protected void error(@NonNull ProcessorException ex) {
        if (ex.getMessage() != null) {
            processingEnv.getMessager().printMessage(Diagnostic.Kind.ERROR, ex.getMessage());
        }
        for (ProcessingError error : ex.getErrors()) {
            error(error);
        }
    }
    
    @Override
    public SourceVersion getSupportedSourceVersion() {
        return SourceVersion.latestSupported();
    }

    @Override
    public Iterable<? extends Completion> getCompletions(
            Element element, AnnotationMirror annotation, ExecutableElement member, String userText) {
        final Prompter globalPrompter = prompters.getGlobalPrompter();
        final Prompter prompter =
                prompters.lookupPrompter(ModelUtils.getAnnotationQualifiedName(annotation).toString());

        final List<Completion> completions = new ArrayList<>();
        if (prompter != null) {
            completions.addAll(prompter.getCompletions(element, annotation, member, userText));
        }
        if (globalPrompter != null && completions.isEmpty()) {
            completions.addAll(globalPrompter.getCompletions(element, annotation, member, userText));
        }
        return completions;
    }

}
