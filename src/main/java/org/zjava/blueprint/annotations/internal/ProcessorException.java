package org.zjava.blueprint.annotations.internal;

import java.util.Arrays;
import java.util.List;
import lombok.Getter;

/**
 *
 * @author Michał Blinkiewicz
 */
public class ProcessorException extends Exception {

    @Getter private final List<ProcessingError> errors;

    public ProcessorException(String message, List<ProcessingError> errors) {
        super(message);
        if (errors.isEmpty()) {
            throw new IllegalArgumentException("No errors!");
        }
        this.errors = errors;
    }

    public ProcessorException(List<ProcessingError> errors) {
        this(null, errors);
    }

    public ProcessorException(String message, ProcessingError... errors) {
        this(message, Arrays.asList(errors));
    }

    public ProcessorException(ProcessingError... errors) {
        this(null, Arrays.asList(errors));
    }

}
