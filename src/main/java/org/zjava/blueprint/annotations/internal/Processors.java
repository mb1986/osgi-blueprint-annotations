package org.zjava.blueprint.annotations.internal;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author Michał Blinkiewicz
 */
public class Processors {

    private final Map<String, List<AnnotationProcessor>> processors = new HashMap<>();

    public void add(String annotationType, AnnotationProcessor processor) {
        List<AnnotationProcessor> processorsList = this.processors.get(annotationType);
        if (processorsList == null) {
            processorsList = new ArrayList<>();
            this.processors.put(annotationType, processorsList);
        }
        processorsList.add(processor);
    }

    public List<AnnotationProcessor> lookup(String annotationType) {
        return Collections.unmodifiableList(processors.get(annotationType));
    }

    public Set<String> getAnnotationTypes() {
        return Collections.unmodifiableSet(processors.keySet());
    }
    
}
