package org.zjava.blueprint.annotations.internal.prompters;

import java.util.Collections;
import java.util.List;
import javax.annotation.processing.Completion;
import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.AnnotationMirror;
import javax.lang.model.element.Element;
import javax.lang.model.element.ExecutableElement;
import lombok.RequiredArgsConstructor;
import org.zjava.blueprint.annotations.attrs.Activation;
import org.zjava.blueprint.annotations.attrs.Id;
import org.zjava.blueprint.annotations.attrs.Scope;
import org.zjava.blueprint.annotations.internal.utils.CompletionUtils;

/**
 *
 * @author Michał Blinkiewicz
 */
@RequiredArgsConstructor
public class GlobalPrompter implements Prompter {

    private final ProcessingEnvironment env;

    @Override
    public String getSupportedAnnotationType() {
        return null; // global prompter / supports all
    }

    @Override
    public List<Completion> getCompletions(Element element, AnnotationMirror annotation, ExecutableElement member, String userText) {
        switch (member.getSimpleName().toString()) {
            case "id":
                return CompletionUtils.wrap(env.getElementUtils().getTypeElement(Id.class.getName()));
            case "activation":
                return CompletionUtils.wrap(env.getElementUtils().getTypeElement(Activation.class.getName()));
            case "scope":
                return CompletionUtils.wrap(env.getElementUtils().getTypeElement(Scope.class.getName()));
            default:
                return Collections.emptyList();
        }
    }

}
