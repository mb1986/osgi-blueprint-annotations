package org.zjava.blueprint.annotations.internal.prompters;

import java.util.List;
import javax.annotation.processing.Completion;
import javax.lang.model.element.AnnotationMirror;
import javax.lang.model.element.Element;
import javax.lang.model.element.ExecutableElement;

/**
 *
 * @author Michał Blinkiewicz
 */
public interface Prompter {

    String getSupportedAnnotationType();

    List<Completion> getCompletions(
            Element element, AnnotationMirror annotation, ExecutableElement member, String userText);
    
}
