package org.zjava.blueprint.annotations.internal.prompters;

import java.util.HashMap;
import java.util.Map;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author Michał Blinkiewicz
 */
public class Prompters {

    @Getter @Setter private Prompter globalPrompter = null;
    private final Map<String, Prompter> prompters = new HashMap<>();

    public void addPrompter(Prompter prompter) {
        final String type = prompter.getSupportedAnnotationType();
        if (type == null) {
            throw new NullPointerException("prompter / supported annotation type");
        }
        prompters.put(type, prompter);
    }

    public Prompter lookupPrompter(String annotationType) {
        return prompters.get(annotationType);
    }
    
}
