package org.zjava.blueprint.annotations.internal.utils;

import javax.lang.model.element.AnnotationMirror;
import javax.lang.model.element.Element;
import lombok.Value;

/**
 *
 * @author Michał Blinkiewicz
 */
@Value
public class AnnotatedElement {

    private final Element element;
    private final AnnotationMirror annotation;
    
}
