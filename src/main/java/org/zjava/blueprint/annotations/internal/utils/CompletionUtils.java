package org.zjava.blueprint.annotations.internal.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import javax.annotation.processing.Completion;
import javax.annotation.processing.Completions;
import javax.lang.model.element.Element;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.TypeElement;

/**
 *
 * @author Michał Blinkiewicz
 */
public class CompletionUtils {

    public static List<Completion> wrap(Collection<String> collection) {
        final List<Completion> result = new ArrayList<>(collection.size());
        for (String option : collection) {
            result.add(Completions.of(option));
        }
        return result;
    }

    public static List<Completion> wrap(TypeElement type) {
        final List<Completion> result = new ArrayList<>();
        for (Element enclosed : type.getEnclosedElements()) {
            if (enclosed.getKind().isField()) {
                if (enclosed.getModifiers().containsAll(Arrays.asList(Modifier.PUBLIC, Modifier.STATIC, Modifier.FINAL))) {
                    if (enclosed.asType().toString().equals(String.class.getName())) {
                        result.add(Completions.of(
                                type.getSimpleName().toString() + "." + enclosed.getSimpleName().toString(),
                                type.getQualifiedName().toString() + "." + enclosed.getSimpleName().toString()));
                    }
                }
            }
        }
        return result;
    }
    
}
