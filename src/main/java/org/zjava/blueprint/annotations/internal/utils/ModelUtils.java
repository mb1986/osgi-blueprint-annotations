package org.zjava.blueprint.annotations.internal.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.lang.model.element.AnnotationMirror;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.Name;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;

/**
 *
 * @author Michał Blinkiewicz
 */
public class ModelUtils {

    public static VariableElement getField(TypeElement enclosing, String name) {
        return (VariableElement) getEnclosedElement(enclosing, name, ElementKind.FIELD);
    }

    public static ExecutableElement getMethod(TypeElement enclosing, String name) {
        return (ExecutableElement) getEnclosedElement(enclosing, name, ElementKind.METHOD);
    }

    public static Element getEnclosedElement(TypeElement enclosing, String name, ElementKind kind) {
        for (Element enclosed : enclosing.getEnclosedElements()) {
            if (enclosed.getKind().equals(kind)
                    && enclosed.getSimpleName().contentEquals(name)) {
                return enclosed;
            }
        }
        return null;
    }

    public static Element getEnclosingElement(Element enclosed, ElementKind kind) {
        Element found;
        while ((found = enclosed.getEnclosingElement()) != null) {
            if (found.getKind().equals(kind)) {
                return found;
            }
        }
        return null;
    }

    // FIXME --- check
    public static AnnotationMirror getAnnotationMirror(Element element, String annotationType) {
        for (final AnnotationMirror mirror : element.getAnnotationMirrors()) {
            if (((TypeElement) mirror.getAnnotationType().asElement())
                    .getQualifiedName().toString().equals(annotationType)) {
                return mirror;
            }
        }
        return null;
    }

    // FIXME --- check
    public static <E extends Element> E getElementByName(Collection<E> collection, String name) {
        for (E e : collection) {
            if (e.getSimpleName().toString().equals(name)) {
                return e;
            }
        }
        return null;
    }
    
    // FIXME --- check
    public static List<AnnotatedElement> getAnnotatedEnclosedElements(
            TypeElement enclosing, ElementKind kind, TypeElement type) {
        List<AnnotatedElement> result = new ArrayList<>();
        for (Element enclosed : enclosing.getEnclosedElements()) {
            if (enclosed.getKind().equals(kind)) {
                for (AnnotationMirror annotation : enclosed.getAnnotationMirrors()) {
                    if (annotation.getAnnotationType().asElement().equals(type)) {
                        result.add(new AnnotatedElement(enclosed, annotation));
                    }
                }
            }
        }
        return result;
    }

    // FIXME --- check
    public static Name getAnnotationQualifiedName(AnnotationMirror annotation) {
        return ((TypeElement) annotation.getAnnotationType().asElement()).getQualifiedName();
    }

}
