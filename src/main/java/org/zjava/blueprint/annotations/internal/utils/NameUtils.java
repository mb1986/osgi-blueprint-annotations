package org.zjava.blueprint.annotations.internal.utils;

import javax.lang.model.element.Name;

/**
 *
 * @author Michał Blinkiewicz
 */
public final class NameUtils {

    public static String fieldToSetter(Name name, boolean noexcept) {
        final String nameStr = name.toString();
        if (nameStr.length() > 0) {
            return "set" + nameStr.substring(0, 1).toUpperCase() + nameStr.substring(1);
        }
        if (noexcept) {
            return null;
        } else {
            throw new IllegalStateException("The field name is empty!");
        }
    }

    public static String setterToField(Name name, boolean noexcept) {
        final String nameStr = name.toString();
        if (nameStr.startsWith("set") && nameStr.length() > 3) {
            return nameStr.substring(3, 4).toLowerCase() + nameStr.substring(4);
        }
        if (noexcept) {
            return null;
        } else {
            throw new IllegalStateException("Could not create a field name for the given method '" + nameStr + "'!");
        }
    }

    //FIXME --- check
    @Deprecated
    public static String classNameToId(Name name) {
        final String nameStr = name.toString();
        return nameStr.substring(0, 1).toLowerCase() + nameStr.substring(1);
    }

    //FIXME --- check
    @Deprecated
    public static String classNameToBeanId(Name name) {
        return classNameToId(name) + "Bean";
    }

}
