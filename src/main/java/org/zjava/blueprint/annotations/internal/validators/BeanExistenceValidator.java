package org.zjava.blueprint.annotations.internal.validators;

import java.util.Map;
import javax.lang.model.element.AnnotationMirror;
import javax.lang.model.element.AnnotationValue;
import javax.lang.model.element.Element;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.TypeElement;
import lombok.RequiredArgsConstructor;
import org.zjava.blueprint.annotations.Bean;
import org.zjava.blueprint.annotations.internal.AnnotationProcessor;
import org.zjava.blueprint.annotations.internal.ProcessingError;

/**
 *
 * @author Michał Blinkiewicz
 */
@RequiredArgsConstructor
public class BeanExistenceValidator extends AnnotationProcessor {

    @Override
    public void process(TypeElement type, Element annotated, AnnotationMirror annotation,
            Map<? extends ExecutableElement, ? extends AnnotationValue> values) {

        Element foundElement = annotated;
        do {
            if (foundElement.getKind().isClass()) {
                if (foundElement.getAnnotation(Bean.class) != null) {
                    return;
                }
            }
        } while ((foundElement = foundElement.getEnclosingElement()) != null);

        error(new ProcessingError("Required 'Bean' annotation is not provided!", annotated, annotation));
    }

}
