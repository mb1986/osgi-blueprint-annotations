package org.zjava.blueprint.annotations.internal.validators;

import java.util.Map;
import javax.lang.model.element.AnnotationMirror;
import javax.lang.model.element.AnnotationValue;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;
import org.apache.commons.lang3.StringUtils;
import org.zjava.blueprint.annotations.Inject;
import org.zjava.blueprint.annotations.internal.AnnotationProcessor;
import org.zjava.blueprint.annotations.internal.ProcessingError;
import org.zjava.blueprint.annotations.internal.utils.ModelUtils;
import org.zjava.blueprint.annotations.internal.utils.NameUtils;

/**
 *
 * @author Michał Blinkiewicz
 */
public class InjectValidator extends AnnotationProcessor {

    @Override
    public void process(final TypeElement type, final Element annotated, final AnnotationMirror annotation,
            final Map<? extends ExecutableElement, ? extends AnnotationValue> values) {

        final Inject inject = annotated.getAnnotation(Inject.class);
        if (StringUtils.isNotBlank(inject.ref()) && StringUtils.isNotBlank(inject.value())) {
            error(new ProcessingError("Both 'ref' and 'value' are set!", annotated, annotation));
        } else if (StringUtils.isBlank(inject.ref()) && StringUtils.isBlank(inject.value())) {
            error(new ProcessingError("Neither 'ref' nor 'value' is set!", annotated, annotation));
        }

        if (inject.enhancedValidation()) {
            final VariableElement field;
            final ExecutableElement setter;
            final TypeElement enclosingClass;
            if (annotated.getKind().isField()) {
                field = (VariableElement) annotated;
                enclosingClass = (TypeElement)
                        ModelUtils.getEnclosingElement(field, ElementKind.CLASS);
                final String setterName = NameUtils.fieldToSetter(field.getSimpleName(), true);
                setter = ModelUtils.getMethod(enclosingClass, setterName);
            } else if (annotated.getKind().equals(ElementKind.METHOD)) {
                setter = (ExecutableElement) annotated;
                enclosingClass = (TypeElement)
                        ModelUtils.getEnclosingElement(setter, ElementKind.CLASS);
                final String fieldName = NameUtils.setterToField(setter.getSimpleName(), true);
                field = ModelUtils.getField(enclosingClass, fieldName);
            } else {
                field = null;
                setter = null;
            }

            if (field != null && setter != null) {
                if (field.getAnnotation(Inject.class) != null
                        && setter.getAnnotation(Inject.class) != null) {
                    error(new ProcessingError("Overlapping 'inject' annotations!", annotated, annotation));
                }
            }
            
            if (setter != null) {
                if (!setter.getSimpleName().toString().startsWith("set")) {
                    error(new ProcessingError("Setter method name does not start with 'set' phrase!", annotated, annotation));
                }
                if (setter.getSimpleName().toString().length() <= 3) {
                    error(new ProcessingError("Found method is not a setter!", annotated, annotation));
                }
                if (setter.getParameters().size() != 1) {
                    error(new ProcessingError("Setter method has incorrect number of parameters!", annotated, annotation));
                }/* else if (!setter.getParameters().get(0).asType().equals(field.asType())) {
                    error(new ProcessingError("Inconsistent field and setter types!", annotated, annotation));
                }*/
            } else {
                error(new ProcessingError("Setter is not provided!", annotated, annotation));
            }
        }
    }
    
}
