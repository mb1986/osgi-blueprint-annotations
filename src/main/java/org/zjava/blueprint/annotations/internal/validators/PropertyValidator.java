package org.zjava.blueprint.annotations.internal.validators;

import java.util.Map;
import javax.lang.model.element.AnnotationMirror;
import javax.lang.model.element.AnnotationValue;
import javax.lang.model.element.Element;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.TypeElement;
import org.apache.commons.lang3.StringUtils;
import org.zjava.blueprint.annotations.Properties;
import org.zjava.blueprint.annotations.Property;
import org.zjava.blueprint.annotations.internal.AnnotationProcessor;
import org.zjava.blueprint.annotations.internal.ProcessingError;

/**
 *
 * @author Michał Blinkiewicz
 */
public class PropertyValidator extends AnnotationProcessor {

    @Override
    public void process(final TypeElement type, final Element annotated, final AnnotationMirror annotation,
            final Map<? extends ExecutableElement, ? extends AnnotationValue> values) {

        final Property property = annotated.getAnnotation(Property.class);
        if (StringUtils.isBlank(property.name())) {
            error(new ProcessingError("'Name' is empty!", annotated, annotation));
        }
        if (StringUtils.isNotBlank(property.ref()) && StringUtils.isNotBlank(property.value())) {
            error(new ProcessingError("Both 'ref' and 'value' are set!", annotated, annotation));
        } else if (StringUtils.isBlank(property.ref()) && StringUtils.isBlank(property.value())) {
            error(new ProcessingError("Neither 'ref' nor 'value' is set!", annotated, annotation));
        }

        if (annotated.getAnnotation(Properties.class) != null) {
            error(new ProcessingError("Conflicting 'Properties' annotation!", annotated, annotation));
        }
    }
    
}
