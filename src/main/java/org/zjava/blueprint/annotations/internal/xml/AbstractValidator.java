package org.zjava.blueprint.annotations.internal.xml;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import lombok.NonNull;

/**
 *
 * @author Michał Blinkiewicz
 */
public abstract class AbstractValidator<T> {

    private final List<ValidationError> errors = new ArrayList<>();

    protected abstract T validateImpl();

    public final T validate() throws ValidatorException {
        final T result = validateImpl();
        if (!errors.isEmpty()) {
            throw new ValidatorException(errors);
        }
        return result;
    }

    protected final void error(@NonNull String messagePattern, Object... arguments) {
        final String message = MessageFormat.format(messagePattern, arguments);
        errors.add(new ValidationError(message));
    }

}
