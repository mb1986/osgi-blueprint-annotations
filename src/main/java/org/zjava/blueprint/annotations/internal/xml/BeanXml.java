package org.zjava.blueprint.annotations.internal.xml;

import java.util.Collections;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;
import org.zjava.blueprint.annotations.attrs.Activation;
import org.zjava.blueprint.annotations.attrs.Scope;

/**
 *
 * @author Michał Blinkiewicz
 */
@XmlRootElement(name = "bean")
@XmlAccessorType(XmlAccessType.NONE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class BeanXml {

    @XmlAttribute
    @Getter private String id = null;
    @XmlAttribute(name = "depends-on")
    @Getter private String dependsOn = null;
    @XmlAttribute(name = "class")
    @Getter private String clazz = null;
    @XmlAttribute(name = "factory-ref")
    @Getter private String factoryComponent = null;
    @XmlAttribute(name = "factory-method")
    @Getter private String factoryMethod = null;
    @XmlAttribute
    @Getter private String activation = null;
    @XmlAttribute
    @Getter private String scope = null;
    @XmlAttribute(name = "init-method")
    @Getter private String initMethod = null;
    @XmlAttribute(name = "destroy-method")
    @Getter private String destroyMethod = null;

    @XmlElementRef
    @Getter private List<PropertyXml> properties = null;

    // TODO --- arguments

    private BeanXml(Validator v) {
        this.id = StringUtils.trimToNull(v.id);
        this.dependsOn = StringUtils.trimToNull(v.dependsOn);
        this.clazz = StringUtils.trimToNull(v.clazz);
        this.factoryComponent = StringUtils.trimToNull(v.factoryComponent);
        this.factoryMethod = StringUtils.trimToNull(v.factoryMethod);
        this.activation = StringUtils.trimToNull(v.activation);
        this.scope = StringUtils.trimToNull(v.scope);
        this.initMethod = StringUtils.trimToNull(v.initMethod);
        this.destroyMethod = StringUtils.trimToNull(v.destroyMethod);
        if (v.properties != null && !v.properties.isEmpty()) {
            this.properties = Collections.unmodifiableList(v.properties);
        }
    }

    /**
     * This class provides a validator implementation for {@link BeanXml}.
     */
    public static final class Validator extends AbstractValidator<BeanXml> {

        @Setter private String id = null;
        @Setter private String activation = null;
        @Setter private String dependsOn = null;
        @Setter private String clazz = null;
        @Setter private String scope = null;
        @Setter private String initMethod = null;
        @Setter private String destroyMethod = null;
        @Setter private String factoryMethod = null;
        @Setter private String factoryComponent = null;
        @Setter private List<PropertyXml> properties = null;

        @Override
        protected BeanXml validateImpl() {
            if (StringUtils.isNotBlank(this.activation)) {
                if (!Activation.PERMITTED.contains(this.activation)) {
                    error("Wrong bean activation value ''{0}''!", this.activation);
                }
            }
            if (StringUtils.isNotBlank(this.scope)) {
                if (!Scope.PERMITTED.contains(this.scope)) {
                    error("Wrong bean scope value ''{0}''!", this.scope);
                }
            }
            if (StringUtils.isNotBlank(this.id)) {
                if (this.id.startsWith("blueprint")) {
                    error("Id can not start with the 'blueprint' phrase!");
                }
            }
            if (StringUtils.equals(this.activation, Activation.EAGER) && StringUtils.equals(this.scope, Scope.PROTOTYPE)) {
                error("Activation and scope validation exception!");
            }
            if (StringUtils.isBlank(this.clazz) && StringUtils.isBlank(this.factoryComponent)) {
                error("Either class or factoryComponent must be set!");
            }
            if (StringUtils.isNotBlank(this.clazz) && StringUtils.isNotBlank(this.factoryComponent)) {
                error("Class and factoryComponent can not be set together!");
            }
            if (StringUtils.isNotBlank(this.factoryComponent) && StringUtils.isBlank(this.factoryMethod)) {
                error("Factory method is not defined!");
            }
            if (StringUtils.equals(this.scope, Scope.PROTOTYPE) && StringUtils.isNotBlank(this.destroyMethod)) {
                error("Destroy method must not be set when the scope is prototype!");
            }
            return new BeanXml(this);
        }

    }
    
}
