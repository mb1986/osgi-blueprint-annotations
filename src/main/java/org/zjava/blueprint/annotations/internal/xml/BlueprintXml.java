package org.zjava.blueprint.annotations.internal.xml;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.RequiredArgsConstructor;

/**
 *
 * @author Michał Blinkiewicz
 */
@XmlRootElement(name = "blueprint")
@XmlAccessorType(XmlAccessType.NONE)
@RequiredArgsConstructor // FIXME
public class BlueprintXml {

    public static final String NAMESPACE = "http://www.osgi.org/xmlns/blueprint/v1.0.0";
    public static final String SCHEMA_LOCATION = "http://www.osgi.org/xmlns/blueprint/v1.0.0/blueprint.xsd";
    public static final String NAMESPACE_SCHEMA_LOCATION = NAMESPACE + " " + SCHEMA_LOCATION;

    @XmlElementRef
    private final List<BeanXml> beans;

    private BlueprintXml() {
        this.beans = null;
    }

    // TODO --- other managers

}
