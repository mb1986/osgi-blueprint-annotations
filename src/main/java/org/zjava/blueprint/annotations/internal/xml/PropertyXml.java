package org.zjava.blueprint.annotations.internal.xml;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author Michał Blinkiewicz
 */
@XmlRootElement(name = "property")
@XmlAccessorType(XmlAccessType.NONE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class PropertyXml {

    @XmlAttribute
    @Getter private String name = null;
    @XmlAttribute
    @Getter private String ref = null;
    @XmlAttribute
    @Getter private String value = null;

    private PropertyXml(Validator v) {
        this.name = StringUtils.trimToNull(v.name);
        this.ref = StringUtils.trimToNull(v.ref);
        this.value = StringUtils.trimToNull(v.value);
    }

    public static final class Validator extends AbstractValidator<PropertyXml> {
        
        @Setter private String name = null;
        @Setter private String ref = null;
        @Setter private String value = null;

        @Override
        protected PropertyXml validateImpl() {
            if (StringUtils.isBlank(name)) {
                error("Name is empty!");
            }
            if (StringUtils.isBlank(ref) && StringUtils.isBlank(value)) {
                error("Neither 'ref' nor 'value' is set!");
            }
            if (StringUtils.isNotBlank(ref) && StringUtils.isNotBlank(value)) {
                error("Both 'ref' and 'value' are set!");
            }
            return new PropertyXml(this);
        }

    }

}
