package org.zjava.blueprint.annotations.internal.xml;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

/**
 *
 * @author Michał Blinkiewicz
 */
@RequiredArgsConstructor
public class ValidationError {

    @Getter @NonNull private final String message;

}
