package org.zjava.blueprint.annotations.internal.xml;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import lombok.NonNull;

/**
 *
 * @author Michał Blinkiewicz
 */
public class ValidatorException extends Exception {

    private final List<ValidationError> errors = new ArrayList<>();

    public ValidatorException(@NonNull String message) {
        this.errors.add(new ValidationError(message));
    }

    public ValidatorException(@NonNull ValidationError error) {
        this.errors.add(error);
    }

    public ValidatorException(@NonNull Collection<ValidationError> errors) {
        super("Validator throws an exception containing " + errors.size() + " error(s)!");
        this.errors.addAll(errors);
    }

    public List<ValidationError> getErrors() {
        return Collections.unmodifiableList(errors);
    }

}
