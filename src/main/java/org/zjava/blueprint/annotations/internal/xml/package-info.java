@XmlSchema(namespace = BlueprintXml.NAMESPACE,
        location = BlueprintXml.SCHEMA_LOCATION,
        elementFormDefault = XmlNsForm.QUALIFIED)
package org.zjava.blueprint.annotations.internal.xml;

import javax.xml.bind.annotation.XmlNsForm;
import javax.xml.bind.annotation.XmlSchema;

